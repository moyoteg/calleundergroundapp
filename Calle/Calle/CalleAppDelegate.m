//
//  CalleAppDelegate.m
//  Calle
//
//  Created by Barima Kwarteng on 10/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CalleAppDelegate.h"
#import "HomeViewController.h"
#import "GamesViewController.h"
#import "ProfileViewController.h"
#import "PreferencesViewController.h"
#import "Login.h"

@implementation CalleAppDelegate

@synthesize window = _window;
@synthesize tabBarController = _tabBarController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //Home Tab
    HomeViewController *home = [[[HomeViewController alloc] initWithNibName:nil bundle:nil] autorelease];
    UINavigationController *homeNav = [[[UINavigationController alloc] initWithRootViewController:home] autorelease];
    UITabBarItem *homeItem = [[[UITabBarItem alloc] initWithTitle:@"Home" image:nil tag:0] autorelease];
    homeNav.tabBarItem = homeItem;
    
    //Games Tab
    GamesViewController *games = [[[GamesViewController alloc] initWithNibName:nil bundle:nil] autorelease];
    UINavigationController *gamesNav = [[[UINavigationController alloc] initWithRootViewController:games] autorelease];
    UITabBarItem *gameItem = [[[UITabBarItem alloc] initWithTitle:@"Games" image:nil tag:0] autorelease];
    gamesNav.tabBarItem = gameItem;
    
    //Profile Tab
    ProfileViewController *profile = [[[ProfileViewController alloc] initWithNibName:nil bundle:nil] autorelease];
    UINavigationController *profNav = [[[UINavigationController alloc] initWithRootViewController:profile] autorelease];
    UITabBarItem *profItem = [[[UITabBarItem alloc] initWithTitle:@"Profile" image:nil tag:0] autorelease];
    profNav.tabBarItem = profItem;

    //Preferences
    PreferencesViewController *pref = [[[GamesViewController alloc] initWithNibName:nil bundle:nil] autorelease];
    UINavigationController *prefNav = [[[UINavigationController alloc] initWithRootViewController:pref] autorelease];
    UITabBarItem *prefItem = [[[UITabBarItem alloc] initWithTitle:@"Preferences" image:nil tag:0] autorelease];
    prefNav.tabBarItem = prefItem;
    
    NSArray *viewControllers = [NSArray arrayWithObjects:homeNav, gamesNav, profNav, prefNav, nil];
    
    Login *login = [[[Login alloc] initWithNibName:nil bundle:nil] autorelease];
    
    [self.tabBarController setViewControllers:viewControllers];
    self.window.rootViewController = self.tabBarController;
    //login.view.frame = CGRectMake(0, 20, 320, 460);
    //[self.window addSubview:login.view];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

- (void)dealloc
{
    [_window release];
    [_tabBarController release];
    [super dealloc];
}

@end
