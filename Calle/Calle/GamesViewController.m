//
//  GamesViewController.m
//  Calle
//
//  Created by Barima Kwarteng on 10/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GamesViewController.h"

@implementation GamesViewController
@synthesize ctrl;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    ctrl = [[UISegmentedControl alloc] initWithFrame:CGRectZero];
    ctrl.segmentedControlStyle = UISegmentedControlStyleBar;
    [ctrl insertSegmentWithTitle: @"Nearby Games" atIndex: 0 animated: NO];
    [ctrl insertSegmentWithTitle: @"My Crew" atIndex: 1 animated: NO];
    [ctrl insertSegmentWithTitle: @"Nearby Crew" atIndex: 2 animated: NO];
    [ctrl setSelectedSegmentIndex:0];
    [ctrl sizeToFit];
    
    self.navigationItem.titleView = ctrl;

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
